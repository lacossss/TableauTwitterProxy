## Installing the client

```bash
virtualenv env
source env/bin/activate
pip install -r requirements.txt

python ./twitter_scraper_p2.py --help
python ./twitter_scraper_p2.py --server='http://<TABPY_SERVER>/'
```


## Running the server

If a standalone tabpy server is desired, the `server-requirements.txt` file can be used to install bot the tabpy-server and the requirements for the twitter sproxy client. (As all packages needed by the client need to be installed on the server also).

So installing the server with `virtualenv`:

```bash
virtualenv server-env
source server-env/bin/activate
pip install -r server-requirements.txt

# Apply patch, see next section, after that:
server-env/lib/python2.7/site-packages/tabpy_server/startup.sh
```

After this, you need to apply the following patch to the `twitterscraper` module.


## Patching `twitterscraper`

To allow for sentiment analysis to be performed on the tweets, and the avoid the install nightmare that is `lxml`, we need to apply two little changes to the `twitterscraper` python module.


A separate diff is provided at `tweet.py.diff` in this repository.
Apply that diff to the `<SERVER ENV>/site-packages/twitterscraper/tweet.py` in the TabPy SERVER environment.


